/**
 * Development web-server
 */ 
import webpack from 'webpack';
import express from 'express';
import config from './webpack.config';
import proxy from 'http-proxy-middleware';
import Agent from 'agentkeepalive';
import path from 'path';
import open from 'open';

const port = 3000;
const compiler = webpack(config);

/* Use proxy to access api */
let keepaliveAgent =  new Agent({
    maxSockets: 100,
    keepAlive: true,
    maxFreeSockets: 10,
    keepAliveMsecs:1000,
    timeout: 60000,
    keepAliveTimeout: 30000 // free socket keepalive for 30 seconds
});

let onProxyRes = function (proxyRes, req, res) {
    let key = 'www-authenticate';
    proxyRes.headers[key] = proxyRes.headers[key] && proxyRes.headers[key].split(',');
};

let options = {
    target: 'http://localhost:50599/',
    changeOrigin: true,
    logLevel: 'debug',
    agent: keepaliveAgent,
    onProxyRes: onProxyRes
};

var app = express();
app.use('/api/*', proxy(options));
app.use(require('webpack-dev-middleware')(compiler, {
  hot: true,
  historyApiFallback: true,
  publicPath: config.output.publicPath,
  headers: { 'Access-Control-Allow-Origin': '*' }
}));
app.use(require('webpack-hot-middleware')(compiler));
//app.use(express.static(path.resolve(__dirname, 'dist')));
 
app.get('*', (req, res) => {
  res.sendFile(path.join( __dirname, './src/index.html')); 
});

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    open(`http://localhost:${port}`);
  }
});