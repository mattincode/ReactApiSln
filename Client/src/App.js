import React from 'react';
import PropTypes from 'prop-types';
import EmployeePage from './components/employee/EmployeePage';
import {NavLink} from 'react-router-dom';
import {withRouter} from 'react-router';
import I18n, {setLanguage} from "redux-i18n";
import {translations} from "../translations/translations";

class App extends React.Component {

    render() {        
        const employeeHeader = "Employee Header";

        return (
            <I18n translations={translations}>
                <div className="container-fluid">
                    <nav className="navbar navbar-default navbar-static-top">
                        <ul className="nav navbar-nav">                    
                            <li><NavLink exact to="/" activeClassName="active">Home</NavLink></li>
                            <li><NavLink exact to="/employees" activeClassName="active">Employees</NavLink></li>
                            <li><NavLink exact to="/about" activeClassName="active">About</NavLink></li>                    
                        </ul>
                    </nav>
                    <h1>React from scratch</h1>    

                    {this.props.children}         
                </div>
            </I18n>
        );
    }
}

App.propTypes = {
    children: PropTypes.object.isRequired
};

export default withRouter(App);