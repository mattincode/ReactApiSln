import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function systemUserReducer(state = initialState.employees, action) {
  switch (action.type) {
    case types.GET_ALL_EMPLOYEES_SUCCESS:
      return action.employees; // Return the loaded employees

    default:
      return state;  // Always return the previous state if no changes!
  }
}
