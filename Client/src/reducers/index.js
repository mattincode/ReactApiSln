import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import employees from './employeeReducer';
import {i18nState} from 'redux-i18n';

const rootReducer = combineReducers({
  employees,
  router: routerReducer,
  i18nState
});

export default rootReducer;
