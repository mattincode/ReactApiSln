/**
 * @file Base functionality for all api server calls. Calls are made including credentials for Windows Authentication.
 * Most basic actions can use this directly and will not need any extra api-classes.
 */
import 'whatwg-fetch'; // Polyfill for ES6 fetch

const ServerApiBaseUrl = (process.env.NODE_ENV === 'production') ? window.location.origin + process.env.API_PATH : process.env.API_PATH ;
const defaultFetchOptions = {
  credentials: 'include',
	headers: {
		'Accept': 'application/json'
	}
};

const postOrPutFetchOptions = {
  credentials: 'include',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  dataType: 'json'
};


/**
 * Http-get handler, is a plain function and will forward the handling of any errors to the caller.
 * @param api : (ex: 'users/me')
 * @returns {Promise} : Returns data as json.
 */
export function get(api) {
  return new Promise((resolve, reject) => {
    fetch(`${ServerApiBaseUrl}${api}`, {...defaultFetchOptions, method: 'get'})
      .then((response) => {
        if (response.ok) {
          response.text().then(txt => {
            let objectData = JSON.parseWithDate(txt);
            resolve(objectData);
          });
          //resolve(response.json());
        } else {
          reject(response.statusText);
        }
      }, (error) => {
        reject(error);
      });
  });
}

export function post(api, data) {
  return new Promise((resolve, reject) => {
    fetch(`${ServerApiBaseUrl}${api}`, {...postOrPutFetchOptions, method: 'post', body:JSON.stringify(data)})
      .then((response) => {
        if (response.ok) {
          response.text().then(txt => {
            let objectData = JSON.parseWithDate(txt);
            resolve(objectData);
          });
          //resolve(response.json());
        } else {
          reject(response.statusText);
        }
      }, (error) => {
        reject(error);
      });
  });
}

export function put(api, data) {
  return new Promise((resolve, reject) => {
    fetch(`${ServerApiBaseUrl}${api}`, {...postOrPutFetchOptions, method: 'put', body:JSON.stringify(data)})
      .then((response) => {
        if (response.ok) {
          response.text().then(txt => {
            let objectData = JSON.parseWithDate(txt);
            resolve(objectData);
          });
          //resolve(response.json());
        } else {
          reject(response.statusText);
        }
      }, (error) => {
        reject(error);
      });
  });
}

export function ajaxDelete(api, data) {
  return new Promise((resolve, reject) => {
    fetch(`${ServerApiBaseUrl}${api}`, {...postOrPutFetchOptions, method: 'delete', body:data})
      .then((response) => {
        if (response.ok) {
          response.text().then(txt => {
            let objectData = JSON.parseWithDate(txt);
            resolve(objectData);
          });
          //resolve(response.json());
        } else {
          reject(response.statusText);
        }
      }, (error) => {
        reject(error);
      });
  });
}


