import React from 'react';
import { Route, Switch } from 'react-router';
import HomePage from './components/home/HomePage';
import AboutPage from './components/about/AboutPage';
import EmployeePage from './components/employee/EmployeePage';
import createHistory from 'history/createBrowserHistory';

export const history = createHistory();
export const Routes = () => {
  return (
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/about" component={AboutPage}/>
        <Route exact path="/employees" component={EmployeePage}/>        
      </Switch>
  );
};