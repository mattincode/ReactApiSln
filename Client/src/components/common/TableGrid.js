import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'; // http://allenfang.github.io/react-bootstrap-table/docs.html
import {bindActionCreators} from 'redux';
import CustomFilter from './CustomFilter';
import {keyCode} from "../../models/constants/keyCodes";

class TableGrid extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.getOptions = this.getOptions.bind(this);
    this.getSelectRowOptions = this.getSelectRowOptions.bind(this);
    this.formatSelector = this.formatSelector.bind(this);
    this.onRowSelect = this.onRowSelect.bind(this);
    this.phoneFormater = this.phoneFormater.bind(this);
    this.emailFormater = this.emailFormater.bind(this);
    this.sexFormater = this.sexFormater.bind(this);
    this.userFormater = this.userFormater.bind(this);
    this.getTableGridSettings = this.getTableGridSettings.bind(this);
    this.onSortChange = this.onSortChange.bind(this);
    this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
    this.saveGridSettings = this.saveGridSettings.bind(this);
    this.getCustomFilter = this.getCustomFilter.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    HTMLCollection.prototype[Symbol.iterator] = Array.prototype.values; // Add iterator on HTML collection for custom DOM-navigation (for x of y)

    this.state = {
      gridId: '',
      defaultSortName: '',
      defaultSortOrder: '',
      columns: [],
      pendingChanges: false
    };
  }

  componentWillMount() {
    //console.log(this.props.systemUser.gridViewsSettingsXML);
    let columns = [];
    let columnSettings;
    let filterDelay = 400;
    let settings = null;
    this.tableRows = [];
    this.setState({
      gridId: this.props.gridId
    });

    let result = this.getTableGridSettings();

    if (result) {
      settings = JSON.parse(result);
      columnSettings = settings.columns;
    }
    else {
      columnSettings = this.props.columns;
    }

    columnSettings.map(function(obj){

      let column = {
        dataField: obj.dataFieldId ? obj.dataFieldId : obj.dataField,
        name: obj.displayName ? obj.displayName : obj.name,
        dataSort: obj.isSortable ? obj.isSortable : obj.dataSort,
        defaultASC: true
      };

      if(obj.isKey){
        column['isKey'] = obj.isKey;
      }

      if(obj.dataFormat){
        if(obj.dataFormat === 'phone'){
          column['dataFormat'] = this.phoneFormater;
        } else if(obj.dataFormat === 'email'){
          column['dataFormat'] = this.emailFormater;
        } else if(obj.dataFormat === 'sex'){
          column['dataFormat'] = this.sexFormater;
        } else if(obj.dataFormat === 'user') {
          column['dataFormat'] = this.userFormater;
        }
      }

      if(obj.width){
        column['width'] = obj.width;
      }

      if(obj.hidden){
        column['hidden'] = obj.hidden;
      }

      if(obj.isFilterable || obj.filter){
        column['filter'] = {'type': 'CustomFilter', 'delay': filterDelay, 'getElement': this.getCustomFilter};
        column['name'] = column.name;
        column['className'] = 'filterableColumn';
      }

      columns.push(column);
    }, this);

    if(settings){
      this.setState({
        defaultSortName: settings.defaultSortName,
        defaultSortOrder: settings.defaultSortOrder
      });
    }
    else{
      this.setState({
        defaultSortName: this.props.settings.defaultSortFieldId ? this.props.settings.defaultSortFieldId : '',
        defaultSortOrder: this.props.settings.defaultSortOrder ? this.props.settings.defaultSortOrder : 'asc'
      });
    }

    this.setState({
      columns: columns,
    });
  }

  componentDidMount() {
    // Add custom table keydown events not supported by the BootstrapTable (for use by parent components to handle keyboard navigation)
    let parent = document.getElementById(this.state.gridId);
    parent.addEventListener('keydown', this.onKeyDown);
  }

  componentDidUpdate() {
    this.tableRows = [];
    // Keep track of sort order by iterating all rows and get all the rows with id and html-table tr
    let parent = document.getElementById(this.state.gridId);
    let rows = parent.querySelectorAll("table")[1].rows; // We assume that the data-table has index 1 (first should be the header)
    for (const row of rows) {
      let id = row.querySelector("td").innerHTML;        // Get the first value on the row, this is assumed to be the id!
      this.tableRows.push({id: id, row: row});
    }
  }

  componentWillUnmount(){
    this.saveGridSettings();
    // Remove custom keydown events
    let parent = document.getElementById(this.state.gridId);
    parent.removeEventListener('keydown', this.onKeyDown);
    this.tableRows = [];
  }

  // Handle keyboard navigation
  onKeyDown(event) {
    if (!this.props.selectedItemIds) return;
    let currentIndex = this.tableRows.findIndex(row => row.id == this.props.selectedItemIds[0]); // Intentional use of == to handle string to number id:s
    if (currentIndex < 0) return;
    let maxIndex = this.tableRows.length - 1;
    let selectedRow = null;
    if (event.keyCode === keyCode.ArrowDown && currentIndex < maxIndex) {
      selectedRow = this.tableRows[currentIndex+1];
    } else if (event.keyCode === keyCode.ArrowUp && currentIndex > 0) {
      selectedRow = this.tableRows[currentIndex-1];
    } else if (event.keyCode === keyCode.Enter || event.keyCode === keyCode.Space) {
      this.tableRows[currentIndex].row.dispatchEvent(new MouseEvent('dblclick', {'view': window, 'bubbles': true, 'cancelable': true}));

    }

    if (selectedRow) {
      selectedRow.row.click();
    }
  }

  getCustomFilter(filterHandler){
    return (
      <CustomFilter filterHandler={filterHandler}/>
    );
  }

  getOptions(){
    return {
      defaultSortName: this.state.defaultSortName,
      defaultSortOrder: this.state.defaultSortOrder,
      pagination: this.props.settings.pagingIsEnabled,
      sizePerPage: this.props.settings.sizePerPage,
      sizePerPageList: this.props.settings.sizePerPageList,
      hideSizePerPage: this.props.settings.hideSizePerPage,
      alwaysShowAllBtns: true,
      sortIndicator: this.props.settings.alwaysShowSortingIndicator, //sortingindicator
      onSortChange: this.onSortChange,
      onRowDoubleClick: this.onRowDoubleClick
    };
  }

  getSelectRowOptions(){
    if(!this.props.settings.rowSelectionEnabled)
      return {};

    return {
      mode: this.props.multiSelect ? 'checkbox' : 'radio',
      className: 'active',
      hideSelectColumn: true,  // enable hide selection column.
      clickToSelect: true,  // you should enable clickToSelect, otherwise, you can't select column.
      onSelect: this.onRowSelect,
      selected: this.props.selectedItemIds
    };
  }

  onRowSelect(row, isSelected, e) {
    if (this.props.onSelected) {
      this.props.onSelected(row);
    }
  }

  onRowDoubleClick(row){
    if (this.props.onDoubleClick) {
      this.props.onDoubleClick(row);
    }
  }

  formatSelector(cell, row){
    switch (this.props.columns.customFormat){
      case 'phone':
      //return phoneFormatter(cell, row)
    }
  }

  phoneFormater(cellValue, row){
    return '<i class="glyphicon glyphicon-earphone"></i> ' + cellValue;
  }

  emailFormater(cellValue, row){
    return '<i class="glyphicon glyphicon-envelope"></i> ' + cellValue;
  }

  sexFormater(cellValue, row){
    return cellValue; //TODO: Add if statement and symbols for each sex
  }

  userFormater(cellValue, row) {
    return '<i class="glyphicon glyphicon-user"></i> ' + cellValue;
  }

  saveGridSettings(){
    if(this.state.pendingChanges) {
      let gridSettings = {
        systemUser: this.props.systemUser.systemUserId,
        gridId: this.props.gridId,
        settingsJson: JSON.stringify(this.state)
      };
      //this.props.actions.saveUserGridSettings(gridSettings);
    }
  }

  getTableGridSettings(){
    if(this.props.systemUser.gridSettingJson) {
      let gridId = this.props.gridId;
      let grid = this.props.systemUser.gridSettingJson.filter(function (obj) {
        return obj.gridId === gridId;
      })[0];

      if (grid !== undefined) {
        return grid.settingsJson;
      }
    }
    return null;
  }

  onSortChange(sortName, sortOrder){
    this.setState({
      pendingChanges: true,
      defaultSortName: sortName,
      defaultSortOrder: sortOrder
    });
  }

  render(){
     return (
        <div id={this.state.gridId}>
          <BootstrapTable
            gridId={this.state.gridId}
            data={this.props.data}
            hover={this.props.settings.hoverEffectEnabled}
            multiColumnSort={this.props.settings.multiSortDimensions}
            options={this.getOptions()}
            pagination={this.props.settings.pagingIsEnabled}
            selectRow={this.getSelectRowOptions()}>
              {this.state.columns.map(function( column, index ){
                const {name, ...newColumn} = column;
                let displayName = column.name;
                return (
                  <TableHeaderColumn
                    key={index}
                    {...newColumn}>
                      {displayName}
                  </TableHeaderColumn>
                );
              })}
          </BootstrapTable>
        </div>
      );}
}

TableGrid.propTypes = {
  systemUser: PropTypes.object,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      customFormat: PropTypes.oneOf(['phone', 'sex']),
      dataFieldId: PropTypes.string.isRequired,
      dataFormat: PropTypes.string,
      displayName: PropTypes.string.isRequired,
      filterType: PropTypes.string, //'SelectFilter', 'NumberFilter', 'CustomFilter'
      hidden: PropTypes.bool,
      isKey: PropTypes.bool,
      isFilterable: PropTypes.bool,
      isSortable: PropTypes.bool,
      width: PropTypes.string,
      //TODO: Group, width,
    }).isRequired
  ),
  data: PropTypes.array,
  gridId: PropTypes.string.isRequired,
  selectedItemIds: PropTypes.array,
  multiSelect: PropTypes.bool,
  onSelected: function (value) {},
  onDoubleClick: function (value) {},
  onKeyDown: PropTypes.func,
  settings: PropTypes.shape({
    alwaysShowSortingIndicator: PropTypes.bool,
    defaultSortFieldId: PropTypes.string,
    defaultSortOrder: PropTypes.string,
    hoverEffectEnabled: PropTypes.bool,
    multiSortDimensions: PropTypes.number,
    pagingIsEnabled: PropTypes.bool,
    pageSize: PropTypes.number,
    rowSelectionEnabled: PropTypes.bool,
    sizePerPage: PropTypes.number,
    sizePerPageList: PropTypes.arrayOf(
      PropTypes.shape({
        text: PropTypes.string.isRequired,
        value: PropTypes.number.isRequired
      })),
    hideSizePerPage: PropTypes.bool
  })

};

TableGrid.defaultProps = {
  columns: {
    filterType: 'TextFilter'
  },
  multiSelect: false,
  settings: {
    alwaysShowSortingIndicator: true,
    hoverEffectEnabled: true,
    multiSortDimensions: 1,
    pagingIsEnabled: true,
    rowSelectionEnabled: true,
    sizePerPage: 200,
    sizePerPageList: [ {
      text: '200', value: 200
    }],
    hideSizePerPage: false
  }
};

function mapStateToProps(state) {
  return {
    systemUser: state.systemUser
  };
}

export default connect(mapStateToProps, null) (TableGrid);
