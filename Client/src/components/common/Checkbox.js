import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Checkbox extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {isChecked: false};
    this.toggleCheckboxChange = this.toggleCheckboxChange.bind(this);
  }

  toggleCheckboxChange() {
    let newState = !this.state.isChecked;
    this.setState({ isChecked: newState });
    this.props.onCheckboxChange(newState);
  }

  render() {
    return (
      <div className="checkbox">
        <label>
          <input className="form-control"
            type="checkbox"
            value={this.props.label}
            checked={this.state.isChecked}
            onChange={this.toggleCheckboxChange}
          />
          {this.props.label}
        </label>
      </div>
    );
  }
}

Checkbox.propTypes = {
  label: PropTypes.string.isRequired,
  onCheckboxChange: PropTypes.func.isRequired,
};

export default Checkbox;
