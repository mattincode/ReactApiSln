import React from 'react';
import PropTypes from 'prop-types';

class CustomFilter extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.filterData = this.filterData.bind(this);
    this.toggelVisibility = this.toggelVisibility.bind(this);

    this.state = {
      active: false,
      value: ""
    };
  }

  filterData(e) {
    if(e.target){
      let value = e.target.value;
      this.setState({
        value: value
      });
      this.props.filterHandler(value);
    }
  }

  toggelVisibility(){
    this.setState({
      active: !this.state.active
    });
  }

  render(){


    return (
      <div>
        <span className={`glyphicon glyphicon-filter grid-filter-icon ${this.state.value!=="" || this.state.active ? 'activeFilter': ''}`} onClick={this.toggelVisibility}/>
        {this.state.active &&
          <input type="text" className="form-control" onChange={this.filterData} value={this.state.value}/>
        }
      </div>
    );
  }
}

CustomFilter.propTypes = {
  filterHandler: PropTypes.func
};

export default (CustomFilter);
