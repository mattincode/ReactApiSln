import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import DatePicker from './DatePicker';
import {isValidDate} from "../../selectors/dateSelectors";


class DateField extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {errors: [], value: this.props.value};
    this.onBlur = this.onBlur.bind(this);
    this.setError = this.setError.bind(this);
    this.validate = this.validate.bind(this);
    this.onChanged = this.onChanged.bind(this);

  }

  // Detect if validate property has changed and do validation!
  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.validate > 0 && (this.props.validate !== nextProps.validate)) {
      let error = this.validate(this.state.value);
      let validationOk = (error === null);
      if (this.props.onValidate)
        this.props.onValidate(this.props.name, validationOk);
    }
  }

  onBlur(date) {
    this.validate(date);
  }

  onChanged(newDate) {
    this.setState({value: newDate});
    if (this.props.onChanged) {
      this.props.onChanged(newDate);
    }
  }

  validate(date) {
    let error = null;
    if (this.props.required && date._i.length === 0) {
      error = this.context.t('VALIDATION_REQUIRED_FIELD');
    } else if (!isValidDate(date, this.context.t, this.props.locale, this.props.minAllowedDate, this.props.maxAllowedDate)) {
      error = this.context.t('VALIDATION_DATE_INVALID');
    }
    this.setError(error);
    return error;
  }

  setError(error) {
    if (error) {
      this.setState({errors:[{id:1, text:error}]}); // Set error text
    } else if (this.state.errors.length > 0) {
      this.setState({errors:[]});                   // Clear errors
    }
  }

  render() {
    let containerClasses = this.state.errors.length === 0 ? `` : `has-error has-feedback`;
    return (
      <div className={containerClasses}>
        <DatePicker placeholder={this.props.placeholder} className="form-control" value={this.state.value} onBlur={this.onBlur} onChangeRaw={this.onChanged} onChange={this.onChanged} minDate={this.props.minAllowedDate} maxDate={this.props.maxAllowedDate} internal/>
        {this.state.errors.map(error =>  {
          return <div key={error.id}><span className="help-block">{error.text}</span></div>;
        })}
      </div>
    );
  }
}

DateField.propTypes = {
  name: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  minAllowedDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  maxAllowedDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  required: PropTypes.bool,
  validate: PropTypes.number,
  onChanged: PropTypes.func,
  onValidate: PropTypes.func,
  locale: PropTypes.string.isRequired
};

DateField.contextTypes = {
  t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    locale: state.systemUser.language.langKey
  };
}

export default connect(mapStateToProps) (DateField);

