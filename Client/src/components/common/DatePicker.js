﻿import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import DatePick from 'react-datepicker';
import moment from 'moment';

//TODO: Document component
//TODO: Build DatePickerField (since we moved all validation out of this component)
class DatePicker extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.handleChange = this.handleChange.bind(this);
    this.handelRawChange = this.handelRawChange.bind(this);
    this.onBlur = this.onBlur.bind(this);

    this.state = {
      selected: this.setMoment(this.props.value),
      acceptedFormats: this.props.locale === "sv-SE" ? ["YYMMDD", "YYYYMMDD"] : ["DDMMYY"],
      currentDate: null,
      locale: this.props.locale,
      formGroupClassName: "",
      selectsStart: this.props.selectsStart,
      selectsEnd: this.props.selectsEnd,
      dropdownMode: "scroll",
      disabled: this.props.disabled
    };
  }

  onBlur(event) {
    let value = moment(event.target.value, this.state.acceptedFormats, this.props.locale);
    if(value.isValid()){
      this.handleChange(value);
    } else {
     // value = new Date(); // If invalid date, set picker to current date (we will still display the faulty text but the picker will use a valid date)
      this.setState({currentDate: new Date()});
    }

    if (this.props.onBlur)
      this.props.onBlur(value);
  }

  setMoment(value){
    if(value === undefined ||value === null){
      return undefined;
    }
    if(this.isString(value)){
      let date = new Date(value);
      return moment(date);
    }
    return moment(value);
  }

  isString(value){
    return typeof value === 'string';
  }

  handleChange(value) {
    if(value === null)
      return;

    this.setState({selected: value, currentDate: value.locale(this.props.locale).format("L"),});
    if(this.props.onChange)
      this.props.onChange(value);
  }

  handelRawChange(value) {
    this.setState({currentDate: value});

    if(this.props.onChangeRaw)
      this.props.onChangeRaw(value);
  }

  render(){
    if (!this.props.internal)
      return <strong>This component(DatePicker) is for internal use only (use DateField/DateRangeField instead)!</strong>;

    return (
        <DatePick
          {...this.state}
          className={this.props.className}
          title={this.props.title}
          onChange={this.handleChange}
          onChangeRaw={this.handelRawChange}
          onBlur={this.onBlur}
          placeholderText={this.props.placeholder}
          value={this.state.currentDate}
          showWeekNumbers
          minDate={this.setMoment(this.props.minDate)}
          maxDate={this.setMoment(this.props.maxDate)}
          startDate={this.setMoment(this.props.startDate)}
          endDate={this.setMoment(this.props.endDate)}
          openToDate={this.state.selected ? this.state.selected : moment()}
        />
    );}
}

DatePicker.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  minDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  maxDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  startDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  endDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  title: PropTypes.string,
  locale: PropTypes.string,
  selectsStart: PropTypes.bool,
  selectsEnd: PropTypes.bool,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: function (value) {},
  onChangeRaw: function (value) {},
  internal: PropTypes.bool
};

DatePicker.defaultProps = {
  title: "DatePicker", //TODO: TRANSLATIONSs
  className: "form-control"
};

function mapStateToProps(state) {
  return {
    locale: state.systemUser.language.langKey
  };
}

export default connect(mapStateToProps) (DatePicker);
