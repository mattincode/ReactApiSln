/**
 * List: A standard list based of TableGrid that handles sorting, editing etc.
 * Width is determined by parent element (eg. place div with bootstrap col-classes as parent)
 * */
//TODO: Fix initial focus
import React from 'react';
import PropTypes from 'prop-types';
import TableGrid from './TableGrid';

class List extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.getSelectedItemIds = this.getSelectedItemIds.bind(this);
    this.onSelected = this.onSelected.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.state = {selectedItem: this.props.selectedItem};
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.selectedItem !== this.state.selectedItem) {
      this.setState({selectedItem: nextProps.selectedItem});
    }
  }

  onSelected(item) {
    this.setState({selectedItem: item});
    if (this.props.onSelected) {
      this.props.onSelected(item);
    }
  }

  onAdd(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.props.onAdd) {
      this.props.onAdd();
    }
  }

  onDelete(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.props.onDelete) {
      this.props.onDelete(this.state.selectedItem);
    }
  }

  onEdit(event) {
    if (event && event.preventDefault) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (this.props.onEdit) {
      this.props.onEdit(this.state.selectedItem);
    }
  }

  getSelectedItemIds() {
    if (!this.props.columnTitles || !this.state.selectedItem) return [];
    let keyColumn = this.props.columnTitles.find(c => c.isKey === true);
    if (!keyColumn) return [];
    let idColumn = keyColumn.dataFieldId;       // TableGrid fieldId
    if (!idColumn) return [];
    return [this.state.selectedItem[idColumn]]; // Return an array with the selected item (since the list do not support multiselect)

  }

  render() {
    let isDeleteEnabled = this.state.selectedItem;
    let isEditEnabled = this.state.selectedItem;
    return (
      <div className="panel panel-default container-fluid">
        <div className="panel-heading panel-heading-sm row">
          <div className="col-md-9"><label className="panel-title">{this.props.title}</label></div>
          {this.props.showToolbar &&
          <div className="col-md-3">
            <button className="btn btn-danger btn-xs pull-right"  tabIndex="0" data-toggle="modal" data-target="#deleteSmsList" disabled={!isDeleteEnabled} onClick={this.onDelete}><span className="glyphicon glyphicon-trash"/></button>
            <button className="btn btn-warning btn-xs pull-right" tabIndex="0" data-toggle="modal" data-target="#editSmsList" disabled={!isEditEnabled} onClick={this.onEdit}><span className="glyphicon glyphicon-pencil"/></button>
            <button className="btn btn-xs btn-success pull-right" tabIndex="0" autoFocus data-toggle="modal" data-target="#addSmsList" disabled={!this.props.enableAddButton} onClick={this.onAdd}><span className="glyphicon glyphicon-plus"/></button>
          </div> }
        </div>
        <TableGrid gridId={this.props.gridId} columns={this.props.columnTitles} data={this.props.data} onSelected={this.onSelected} selectedItemIds={this.getSelectedItemIds()} onDoubleClick={this.onEdit}/>
      </div>
    );
  }
}

List.propTypes = {
  gridId: PropTypes.string.isRequired,      // gridId is used when saving settings and for DOM-manipulation
  title: PropTypes.string,
  columnTitles: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  selectedItem: PropTypes.object,           // selectedItem is used to pre-select an item in the list
  enableAddButton: PropTypes.bool,
  showToolbar: PropTypes.bool,
  showFilter: PropTypes.bool,
  onAdd: PropTypes.func,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func,
  onSelected: PropTypes.func,
};

List.defaultProps = {
  enableAddButton: true,
  showToolbar: true
};

List.contextTypes = {
  t: PropTypes.func.isRequired
};

export default List;

