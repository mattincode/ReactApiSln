import React from 'react';
import PropTypes from 'prop-types';

// Below must be valid input type attributes, see: https://www.w3schools.com/tags/att_input_type.asp (otherwise, use a selector in the type when setting type on the input-field)
export const InputType = {
  Text: "text",
  Number: "number",
  EMail: "email",
  Phone: "tel"
};

class Input extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {errors: [], value: this.props.value};
    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.validateText = this.validateText.bind(this);
    this.validateTelephoneNumber = this.validateTelephoneNumber.bind(this);
    this.validateEmailAddress = this.validateEmailAddress.bind(this);
    this.validateNumber = this.validateNumber.bind(this);
    this.validate = this.validate.bind(this);
    this.setError = this.setError.bind(this);
  }

  componentDidMount() {
    if (this.props.initialFocus) {
      let that = this;
      setTimeout(() => {
        that.textInput.select();
        that.textInput.focus();
        },
        500); // setTimeout is a workaround to get focus to work in fading modal dialogs!
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    // Check if validation property is enabled!
    if (nextProps.validate > 0 && (this.props.validate !== nextProps.validate)) {
      let error = this.validate();
      let validationOk = (error === null);
      if (this.props.onValidate)
        this.props.onValidate(this.props.name, validationOk);
    }
  }

  onChange(e) {
    this.setState({value: e.target.value});
    if (this.props.onChange) {
      this.props.onChange(e);
    }
  }

  // Perform validation when the user leaves the field
  onBlur(e) {
    this.validate();
  }

  validate() {
    let error = null;
    let value = this.state.value;
    switch (this.props.type) {
      case InputType.Text:
        error = this.validateText(value);
        break;
      case InputType.Phone:
        error = this.validateTelephoneNumber(value);
        break;
      case InputType.EMail:
        error = this.validateEmailAddress(value);
        break;
      case InputType.Number:
        error = this.validateNumber(value);
        break;
    }
    return error;
  }

  setError(error) {
    if (error) {
      this.setState({errors:[{id:1, text:error}]}); // Set error text
    } else if (this.state.errors.length > 0) {
      this.setState({errors:[]});                  // Clear errors
    }
  }

  validateText(text) {
    let error = null;
    if (text) {
      error = (this.props.minLength <= text.length && text.length <= this.props.maxLength) ? null : this.context.t('VALIDATION_TEXT_RANGE').replace("{0}", ` ${this.props.minLength} - ${this.props.maxLength} `)  ;
    } else if (this.props.required) {
      error = this.context.t('VALIDATION_REQUIRED_FIELD');
    }
    this.setError(error);
    return error;
  }

  validateNumber(number) {
    let error = null;
    if (number) {
      error = (this.props.minLength <= number && number <= this.props.maxLength) ? null : `${this.context.t('VALIDATION_NUMBER_RANGE')} ${this.props.minLength} - ${this.props.maxLength}`;
    } else if (this.props.required) {
      error = this.context.t('VALIDATION_REQUIRED_FIELD');
    }
    this.setError(error);
    return error;
  }

  validateEmailAddress(emailAddress) {
    let error = null;
    if (emailAddress) {
      error = (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailAddress)) && emailAddress.length <= this.props.maxLength ? null : this.context.t('VALIDATION_EMAIL');
    } else if (this.props.required) {
      error = this.context.t('VALIDATION_REQUIRED_FIELD');
    }
    this.setError(error);
    return error;
  }

  validateTelephoneNumber(telephoneNumber) {
    let error = null;
    if (telephoneNumber) {
      error = (/^((\+\d+)|00\d+)?([ -]?(\d+|(\(\d+\))))+$/.test(telephoneNumber)) && telephoneNumber.length <= this.props.maxLength ? null : this.context.t('VALIDATION_PHONE_NUMBER');
    } else if (this.props.required) {
      error = this.context.t('VALIDATION_REQUIRED_FIELD');
    }
    this.setError(error);
    return error;
  }

  render() {
    let props = this.props;
    let containerClasses = this.state.errors.length === 0 ? `` : `has-error has-feedback`;

    // Allow the use of arrow function for saving ref since this is recommended by the react-docs over using a string-ref.
    /* eslint-disable react/jsx-no-bind */
    return (
      <div className={containerClasses}>
        <input ref={(input => {this.textInput = input;})} type={props.type} className={props.className} name={props.name} value={this.state.value} onChange={this.onChange} onBlur={this.onBlur} placeholder={props.placeholder} required={props.required} />
        {this.state.errors.map(error =>  {
          return <span key={error.id} className="help-block">{error.text}</span>;
        })}
      </div>
    );
    /* eslint-enable react/jsx-no-bind */
  }
}

Input.propTypes = {
  type: PropTypes.string,
  minLength: PropTypes.number,
  maxLength: PropTypes.number,
  required: PropTypes.bool,
  className: PropTypes.string,
  onChange: PropTypes.func,
  onValidate: PropTypes.func,
  value: function() {},
  placeholder: PropTypes.string,
  name: PropTypes.string,
  validate: PropTypes.number,
  initialFocus: PropTypes.bool
};

Input.defaultProps = {
  type: InputType.Text,
  minLength: 0,
  maxLength: 9999999,
  required: false,
  className: "form-control input-sm",
  placeholder: "",
  name: ""
};

Input.contextTypes = {
  t: PropTypes.func.isRequired
};

export default Input;
