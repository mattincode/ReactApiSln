import React from 'react';
import PropTypes from 'prop-types';

class Select extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {errors: []};
    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.validate = this.validate.bind(this);
  }

  onChange(e) {
    if (this.props.onChange) {
      this.props.onChange(e);
    }
  }

  // Perform validation when the user leaves the field
  onBlur(e) {
    this.validate(e.target.value);
  }

  validate(value) {
    let error = null;
    if (this.props.isRequired && !(value && value.length > 0)) {
      error = this.context.t('VALIDATION_REQUIRED_FIELD');
    }
    this.setError(error);
  }

  setError(error) {
    if (error) {
      this.setState({errors:[{id:1, text:error}]}); // Set error text
    } else if (this.state.errors.length > 0) {
      this.setState({errors:[]});                   // Clear errors
    }
  }

  render() {
    let props = this.props;
    let containerClasses = this.state.errors.length === 0 ? `` : `has-error has-feedback`;

    return (
        <div className={containerClasses}>
          <select className="form-control" style={{height:"31px", paddingTop:"4px", paddingBottom:"4px"}} selected={props.selected} defaultValue={props.defaultValue} onChange={this.onChange} onBlur={this.onBlur}>
            {props.items}
          </select>
          {this.state.errors.map(error =>  {
            return <span key={error.id} className="help-block">{error.text}</span>;
          })}
        </div>
      );
    }
}

/* Validate properties */
Select.propTypes = {
  items: PropTypes.array.isRequired,
  selected: PropTypes.number.isRequired,
  isRequired: PropTypes.bool,
  defaultValue: PropTypes.number,
  onChange: PropTypes.func,
};

/* Validate context (t is translation) */
Select.contextTypes = {
  t: PropTypes.func.isRequired
};

/* Export the class and connect it to the redux store */
export default Select;

