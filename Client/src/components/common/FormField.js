import React from 'react';
import PropTypes from 'prop-types';

const FormField = ({children, label, propertyName}) => {
  return (
    <div className="form-group">
      <label htmlFor={propertyName} className="control-label col-xs-2">{label}</label>
      <div className="col-xs-10 col-md-8 col-lg-6">
        {children}
      </div>
    </div>
  );
};

/* Validate properties */
FormField.propTypes = {
  label: PropTypes.string.isRequired,
  propertyName: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.element
  ]).isRequired
};

export default FormField;

