import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Icons from '../../images/Icons';
import * as employeeActions from '../../actions/employeeActions';

class EmployeePage extends React.Component {
    
    componentWillMount() {
       this.props.employeeActions.loadEmployees();
    }

    render() {        

        const employees = this.props.employees.length === 0 ? "Loading employees..." : this.props.employees.map(emp => <li key={emp.id}>{emp.name}</li>);

        return (
        <div className="container-fluid">                        
            <h2 className="jumbotron">Employees <img src={Icons.Diagram} width="48" height="48"/></h2>
            <ul>
                {employees}
            </ul>
        </div>
        );
    }
}

EmployeePage.propTypes = {   
  employeeActions: PropTypes.object,
  employees:PropTypes.array 
};

function mapStateToProps(state, ownProps) {
  return {
    employees: state.employees
  };
}

function mapDispatchToProps(dispatch) {
  return {
    employeeActions: bindActionCreators(employeeActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeePage);