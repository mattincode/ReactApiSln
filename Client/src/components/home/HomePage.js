/**
 * HomePage - Example of React stateless component
 * A stateless component is more lightweight and only renders content without any internal state-management
 */
import React from 'react';
import PropTypes from 'prop-types';
import Input, {InputType} from '../common/Input';

const HomePage = () => {

  return (
    <div className="container-fluid">
        <h1>HomePage</h1>
        <Input Type={InputType.Text} required/>
    </div>
  );
};

HomePage.propTypes = {
};

export default HomePage;