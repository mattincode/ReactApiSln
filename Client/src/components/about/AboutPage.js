/**
 * AboutPage - Example of React stateless component
 * A stateless component is more lightweight and only renders content without any internal state-management
 */
import React from 'react';
import PropTypes from 'prop-types';

const AboutPage = () => {

  return (
    <div className="container-fluid">
        <h1>About</h1>
        <div className="Jumbotron">This is a simple application</div>
    </div>
  );
};

AboutPage.propTypes = {
  title: PropTypes.string.isRequired
};

export default AboutPage;