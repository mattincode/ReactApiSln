import * as types from './actionTypes';
import {ajaxPostAction, ajaxGetAction} from './ajaxActions';
//import {getEmployees} from '../api/mockEmployeeApi';

export function getAllEmployeesSuccess(employees) {
  return { type: types.GET_ALL_EMPLOYEES_SUCCESS, employees};
}

/**
 * Dispatches loading of the current user.
 * @returns {Function}
 */
export function loadEmployees() {
  return ajaxGetAction('employee/getAllEmployees', getAllEmployeesSuccess);
  //return getEmployees(getAllEmployeesSuccess);  // Get users using mock-api
}


