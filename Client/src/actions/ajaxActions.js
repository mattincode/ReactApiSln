import * as types from './actionTypes';
import {get, post, put, ajaxDelete} from '../api/apiBase';

/**
 * Action that is used to notify the system that an async operation is ongoing
 * Reducer for ajax-calls will handle ending the request on success-actions.
 * @returns {{type}}
 */
export function beginAjaxCall() {
  return {type: types.BEGIN_AJAX_CALL};
}

/**
 * Action that is used to notify the system that the call has ended and an error occurred.
 * @returns {{type}}
 */
export function ajaxCallError() {
  return {type: types.AJAX_CALL_ERROR};
}

/**
 * Wraps an ajax get request with notification and error handling. 
 * Uses baseApi to perform the actual get.
 * @param api : Api to call ex. '/users/me'.
 * @param onSuccess : Function that will be dispatched with the json-response.
 * @param extra : Extra parameter that will be passed to the onSuccess function
 * @returns {Function}
 */
export function ajaxGetAction(api, onSuccess, extra) {
  return function(dispatch) {
    dispatch(beginAjaxCall());
    get(api).then(jsonResponse => {
      if (extra) {
        dispatch(onSuccess(jsonResponse, extra));
      } else {
        dispatch(onSuccess(jsonResponse));
      }
    }).catch(error => {
      dispatch(ajaxCallError(error));
    });
  };
}

export function ajaxPostAction(api, data, onSuccess, extra) {
  return function(dispatch) {
    dispatch(beginAjaxCall());
    post(api, data).then(jsonResponse => {
      if (extra) {
        dispatch(onSuccess(jsonResponse, extra));
      } else {
        dispatch(onSuccess(jsonResponse));
      }
    }).catch(error => {
      dispatch(ajaxCallError(error));
    });
  };
}

export function ajaxPutAction(api, data, onSuccess) {
  return function(dispatch) {
    dispatch(beginAjaxCall());
    put(api, data).then(jsonResponse => {
      dispatch(onSuccess(jsonResponse));
    }).catch(error => {
      dispatch(ajaxCallError(error));
    });
  };
}

export function ajaxDeleteAction(api, data, onSuccess) {
  return function(dispatch) {
    dispatch(beginAjaxCall());
    ajaxDelete(api, data).then(jsonResponse => {
      dispatch(onSuccess(jsonResponse));
    }).catch(error => {
      dispatch(ajaxCallError(error));
    });
  };
}

