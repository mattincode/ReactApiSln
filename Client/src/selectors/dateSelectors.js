import moment from 'moment';

//TODO: Investigate if correct format corresponds to moment format!
export function isValidDate(date, t, locale, minDate, maxDate) {
  if(date === "" || date === null){
    return true; // Empty date is valid!
  }
  let acceptedFormats = (locale === "sv-SE") ? ["YYMMDD", "YYYYMMDD"] : ["DDMMYY"]; // TODO - We should use translation instead!
  let value = moment(date, acceptedFormats, locale);
  return (value.isValid() && (!maxDate || value <= maxDate) && (!minDate || value >= minDate));
}
