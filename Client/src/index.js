import 'babel-polyfill';                                    // https://babeljs.io/docs/usage/polyfill/
import '../node_modules/bootstrap/dist/js/bootstrap.min';   // Use bootstrap classes
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './styles/styles.css';                               // Make application styles accessable in the entire application
import JsonDateExtensions from 'json.date-extensions';      // Use json-date extensions to use real dates
import ReactDom from 'react-dom';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import {history, Routes} from './routes';
import {ConnectedRouter} from 'react-router-redux';
import App from './App';

const store = configureStore();

// Example: Dispatch any actions that needs to be dispatched before any UI is loaded
//store.dispatch(loadProfitCentersWithOrganizations());

// Override default JSON parsing so we can detect strings containing dates and convert them to instances of the Date class
JSON.useDateParser();

ReactDom.render(
    <Provider store={store}>
        <ConnectedRouter history={history}> 
            <App>
                <Routes/>      
            </App>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('app')
);