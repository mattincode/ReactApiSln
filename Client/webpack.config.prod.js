﻿import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import CopyPlugin from 'copy-webpack-plugin';

const GLOBALS = {
    'process.env.NODE_ENV': JSON.stringify('production'),
    'process.env.API_PATH': JSON.stringify('/api/')
};

module.exports = {
  devtool: 'source-map',
  entry: './src/index',  
  output: {
    path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: './dist'
  },
  target: 'web',
  plugins: [
    new webpack.LoaderOptionsPlugin({
      noInfo: false,
      debug: true
    }),
    new webpack.DefinePlugin(GLOBALS),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new ExtractTextPlugin('styles.css'),
    new webpack.optimize.UglifyJsPlugin({minimize: true}),
    new CopyPlugin([
      {from: 'node_modules/jquery/dist/jquery.min.js', to: 'jquery.js'},
    ])
  ],
  module: {
    rules: [
      {test: /\.js$/, include: path.join(__dirname, 'src'), use: [{ loader: "babel-loader" }] },
      {test: /(\.css)$/, use: ExtractTextPlugin.extract({use: "css-loader"})},
      {test: /\.(woff|woff2)$/, use: [{loader:"url-loader", options: {prefix: "font", limit: 5000}}]},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:'file-loader'}]},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:"url-loader", options: {limit: 10000, mimeType: "application/octet-stream"}}]},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:"url-loader", options: {limit: 10000, mimeType: "image/svg+xml"}}]},
      {test: /\.(jpg|png|gif)$/, include: path.join(__dirname, 'src/images'), use: [{loader: "url-loader", options: {limit: 25000}}]}
    ]
  }
};
