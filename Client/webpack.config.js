import webpack from 'webpack';
import path from 'path';
import CopyPlugin from 'copy-webpack-plugin';

// With DefinePlugin we can make environment variables accessable to the client
const GLOBALS = {
    'process.env.NODE_ENV': JSON.stringify('development'),
    'process.env.API_PATH': JSON.stringify('http://localhost:50599/api/'),
};

module.exports = {
  devtool: 'source-map',
  entry: [
    'eventsource-polyfill',                       // Necessary for hot reloading with IE
    'webpack-hot-middleware/client?reload=true',  // Reloads the page if hot module reloading fails.
    './src/index'
  ],  
  devServer: {
    contentBase: './src'
  },    
  target: 'web',
  output: {
    path: __dirname + '/dist',                  // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(), 
    new webpack.DefinePlugin(GLOBALS),   
    new webpack.LoaderOptionsPlugin({
      noInfo: false,
      debug: true
    }),    
    new CopyPlugin([
      {from: 'node_modules/jquery/dist/jquery.js', to: 'jquery.js'},
    ])
  ],
  module: {
    rules: [
      {test: /\.js$/, include: path.join(__dirname, 'src'), use: [{loader: "babel-loader"}]},
      {test: /(\.css)$/, use: [{loader:'style-loader'}, {loader:'css-loader'}]},
      {test: /\.(woff|woff2)$/, use: [{loader:"url-loader", options: {prefix: "font", limit: 5000}}]},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:'file-loader'}]},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:"url-loader", options: {limit: 10000, mimeType: "application/octet-stream"}}]},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:"url-loader", options: {limit: 10000, mimeType: "image/svg+xml"}}]},
      {test: /\.(jpg|png|gif)$/, include: path.join(__dirname, 'src/images'), use: [{loader: "url-loader", options: {limit: 25000}}]}
    ]
  }
};