// This script copies src/index.html into /dist/index.html
// This is a good example of using Node and cheerio to do a simple file transformation.
// In this case, the transformation is useful since we only use a separate css file in prod.
import fs from 'fs';
import path from 'path';
import cheerio from 'cheerio';
import colors from 'colors';

/*eslint-disable no-console */
const htmlPath = path.join(__dirname, '../src', 'index.html');

fs.readFile(htmlPath, 'utf8', (err, markup) => {
  if (err) {
    return console.log(err);
  }
  const $ = cheerio.load(markup.replace(/^\uFEFF/, '')); // Remove BOM that is inserted in the markup
  // since a separate spreadsheet is only utilized for the production build, need to dynamically add this here.
  $('head').prepend('<link rel="stylesheet" href="styles.css" type="text/css">');
  fs.writeFile('dist/index.html', $.html(), 'utf8', function (err) {
    if (err) {
      return console.log(err);
    }
    console.log('index.html written to /dist'.green);
  });
});
