import express from 'express';
import path from 'path';
import open from 'open';
import compression from 'compression';
import proxy from 'http-proxy-middleware';
import Agent from 'agentkeepalive';

/*eslint-disable no-console */

const port = 3000;
const app = express();

/* Use proxy to access api */
let keepaliveAgent = new Agent({
    maxSockets: 100,
    keepAlive: true,
    maxFreeSockets: 10,
    keepAliveMsecs: 1000,
    timeout: 60000,
    keepAliveTimeout: 30000 // free socket keepalive for 30 seconds
});

let onProxyRes = function (proxyRes, req, res) {
    let key = 'www-authenticate';
    proxyRes.headers[key] = proxyRes.headers[key] && proxyRes.headers[key].split(',');
};

let options = {
    target: 'http://localhost:50599/',
    changeOrigin: true,
    logLevel: 'debug',
    agent: keepaliveAgent,
    onProxyRes: onProxyRes
};

app.use('/api/*', proxy(options));

app.use(compression());
app.use(express.static('dist'));

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, '../dist/index.html'));
});

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    open(`http://localhost:${port}`);
  }
});
