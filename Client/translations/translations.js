export const translations = {
    'en': {
        'VALIDATION_REQUIRED_FIELD': 'Field is required',
        'VALIDATION_TEXT_RANGE': 'Text must be between {0} characters',
        'VALIDATION_NUMBER_RANGE': 'Number must be in the range',
        'VALIDATION_EMAIL': 'Invalid email address',
        'VALIDATION_PHONE_NUMBER': 'Invalid phone number',
        'VALIDATION_DATE_STARTFORMAT': 'Invalid startdate format!',
        'VALIDATION_DATE_ENDFORMAT': 'Invalid enddate format!',
        'VALIDATION_DATE_STARTEND': 'Startdate later than enddate!',
        'VALIDATION_DATE_MISSING_START': 'Startdate is required!',
        'VALIDATION_DATE_MISSING_END': 'Enddate is required!',
        'VALIDATION_DATE_INVALID': 'Invalid date!'
    }
};