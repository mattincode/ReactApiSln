# React-Redux starter kit with a C# WebAPI 
This is a starter kit for using a Microsoft Web-api with a react-redux client.
The client uses React-Redux, Webpack, ES2015, Bootstrap with JQuery-support. The project also handles dates and json camel-casing correctly!

## Background
Most Microsoft developers want a File->New Project type experience when doing development. Currently (VS2017 update 2) MS is working on a solution to use IIS Express/Kestrel with hot module reloading in a react-template. This would enable the same web-server to serve the web api. The state of this work (from my understanding) is that there's an abstraction that rely on a certain version of Webpack. Due to the MS-specific abstraction-layer and the fixed Webpack version and a very bloated project it's a very confusing experience for new developers.
This project uses a more vanilla approach using standard technology with no extra abstractions to get up and running fast. 

To get to know the basics behind react, redux and webpack it's recommended to instead start with the [reactfromscratch](https://gitlab.com/titte71x/reactfromscratch) project.

## Prerequisites
Below basic technologies are needed if not already installed:
* **Install [Node 6](https://nodejs.org)**. Need to run multiple versions of Node? Use [nvm](https://github.com/creationix/nvm) or [nvm-windows](https://github.com/coreybutler/nvm-windows)
* **Install babel-cli** by running `npm install babel-cli -g` on the commandline
* **Install [git](https://git-scm.com/downloads)**
* **Install [VS2017](https://www.visualstudio.com/downloads/)**
* **Install [VS Code](https://code.visualstudio.com/)**

## Get Started
*Se prerequisites above to get basic tech in place!*
* **Clone this repo using:** `git clone git@gitlab.com:titte71x/ReactApiSln.git`
* **Open `ReactSln.sln` in VS2017 (used for web api-dev)**
* **Start the web api with `F5` (first start will download all packages)**
* **Navigate to `Client` folder and open in VS Code (used for react-dev)**
* **Open the integrated terminal i VS Code and type `npm install`**
* **Start the client with `npm start`**

If above steps was followed correctly a simple page will start displaying a list of employees in the default browser. Any changes of the files in the `src`-folder will update automatically in the browser using webpack Hot Module Reloading(HMR).

## Development Workflow
Typically you will start the web-api in the morning with `F5` and then the client with `npm start`. After that you will edit and make changes during the day, do checkin of code but will rarely need to restart the client. 

### Debug in VS Code (when running)
* Open `Client` folder in VS Code.
* Install `Chrome Debug` extension (Ctrl+Shift+P, ext install + enter, -> 'Debugger for Chrome')
* Press debug and use config `Launch Chrome against localhost` (defined in launch.json).

### Api calls
Api calls are made to the express-dev server and then proxied over to the web api running on a different port.

### Source Control
When adding this project to source control remove the `.git` folder in the main folder.

#### Checking in/out
When using tfs, the simplest is to do all checkin and workitem-association in VS2017. You should install a plugin in VS Code to handle automatic checkout. When using git I do prefer to use the integrated terminal in VS Code to perform commits instead. 

*Note* TFS can use git as source control, you still get all the other stuff!

## Production Build
This project doesn't currently include a production build but it will propably be added if anyone request it. Deployment of the project could be done on any web-server. In his case the webapi should be placed in a virtual sub-folder(IIS) and both client and api will be served from the same webserver and port (hence no cross-domain requests).

## Folder structure
```
ReactApiSln 
|
|---WebApi                      (Standard VS2017 WebAPI-project)
|
|---Client
    |   readme.md               (This file)
    |   package.json            (npm package file, defines dependencies and npm scripts)
    |   .babelrc                (Babel transpiler configuration file, defines how the javascript should be transpiled)
    |   .tfignore               (Team Services file for exluding node_modules and build artefacts)
    |   webpack.config.js       (Webpack configuration used for development builds)
    |---node_modules     
    |   (Do not modify, added using "npm install" only)            
    |---src                     (Source code)
        |---actions             (Redux actions - dispatches api-calls)
        |---api                 (Api mock+server calls)
        |---components          (Components for presentation and composition, This is the bulk of the application logic)
        |---reducers            (Redux reducers, update state based on actions)
        |---selectors           (Transforms data in the shape that different views consume it)
        |---store               (Redux store, holds the application state)
        |---styles              (Global CSS-styles, fonts, colors and theme for the entire application)
        index.html              (Main page, Client entrypoint (app-element))
        index.js                (Bootstraps the HTML-clients routing and store)
        server.js               (Express dev-server using webpack for HMR)
```

### Production Dependencies (Client)

| **Dependency** | **Use** |
|:----------|:-------|
|babel-polyfill | Polyfill for Babel features that cannot be transpiled |
|bootstrap|CSS Framework |
|jquery|Used to support bootstrap modals |
|json.date-extensions| Used for custom date formatting |
|react|React library |
|react-dom|React library for DOM rendering |
|react-redux|Redux library for connecting React components to Redux |
|react-router|React library for routing |
|redux|Library for unidirectional data flows |
|redux-thunk|Async redux library|
|whatwg-fetch|HTTP communication polyfill for fetch|

### Development Dependencies (Server)
  The only global decencies (that must be installed by a developer) are *node* and *babel*. The remaining dependencies should be installed upon *npm install* and be referenced by it's node_modules path.
  
| **Dependency** | **Use** |
|:----------|:-------|
|agentkeepalive|Used to keep connectioon alive during NTLM negotiation when using webpack with proxy in development | 
|babel-cli|Babel Command line interface |
|babel-core|Babel Core for transpiling the new JavaScript to old |
|babel-loader|Adds Babel support to Webpack |
|babel-preset-stage-2| Adds support for spread operator etc.|
|babel-preset-es2015| Babel preset for ES2015|
|babel-preset-react| Add JSX support to Babel |
|babel-preset-react-hmre|Hot reloading preset for Babel|
|copy-webpack-plugin| Used in webpack to copy files outside bundle like Silverlight-xap |
|css-loader|Add CSS support to Webpack|
|eventsource-polyfill|Polyfill to support hot reloading in IE|
|express|Serves development and production builds|
|file-loader| Adds file loading support to Webpack |
|http-proxy-middleware| Proxy used to be able to use different webserver for API during development |
|open|Open app in default browser|
|redux-devtools-extension | Add developer tool support in browsers, used as a middleware during development |
|redux-immutable-state-invariant|Warn when Redux state is mutated|
|style-loader| Add Style support to Webpack (load css and plugins as postcss) |
|url-loader| Add url loading support to Webpack (resolves url:s in css files etc.) |
|webpack| Bundler with plugin system and integrated development server |
|webpack-dev-middleware| Adds middleware support to webpack |
|webpack-hot-middleware| Adds hot reloading to webpack |

## Visual Studio Code

### Tips & tricks
* This readme is best viewed in preview-mode, press preview icon in top right of this window.
* To open Visual studio Code from current folder on the commandline press `code .`


### Keyboard shortcuts
| **Shortcut** | **Function** |
|:----------|:-------|
|Ctrl+Ö|Open integrated terminal|


### Known issues
* `Microsoft Edge` is very slow to update with Hot Module Reloading if Chrome is not opened on the same page. Use Chrome for the best experience, including better dev-plugins for react/redux.
* Sometimes when adding files `VS2017` changes the `Client.csproj`-file to include all distinct files instead of `./src*`. This will make it hard to add new files and folders in VS Code or any other editor. If this happen, edit the csproj manually. You should rarely add files outside the src-folder in the client (only config-stuff) so it should not be a big issue.

## Future additions
This project is missing some parts to be a complete solution for a large scale web-project that can be added in the future:
* Linting
* Routing (client and server side)
* Production build (compression, css bundling, remove dev-stuff)
* Translation
* Css-modules
* PostCss
* Editor config
