﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Http.Cors;

namespace WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("http://localhost:3000", "*", "*");
            cors.SupportsCredentials = true;
            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var serializerSettings = config.Formatters.JsonFormatter.SerializerSettings;
            serializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Utc;
            serializerSettings.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat;
            serializerSettings.DateFormatString = "yyyy-MM-ddTHH:mm:ss.fffffffZ";
            serializerSettings.ContractResolver = StampObjectContractResolver.Instance;
            serializerSettings.Converters = new List<JsonConverter>(new JsonConverter[] { new UpdateStampObjectConverter() });
        }
    }

    class StampValueProvider : IValueProvider
    {
        private bool isForCreatedOn;

        public StampValueProvider(bool isForCreatedOn)
        {
            this.isForCreatedOn = isForCreatedOn;
        }

        public object GetValue(object target)
        {
            if (isForCreatedOn)
            {
                return ((ICreateStampObject)target).CreatedOn.ToUniversalTime();
            }
            else
            {
                return ((IUpdateStampObject)target).ModifiedOn.ToUniversalTime();
            }
        }

        public void SetValue(object target, object value)
        {
        }
    }

    class StampDateTimeAsLongValueProvider : IValueProvider
    {
        private bool isForCreatedOn;

        public StampDateTimeAsLongValueProvider(bool isForCreatedOn)
        {
            this.isForCreatedOn = isForCreatedOn;
        }

        public object GetValue(object target)
        {
            if (isForCreatedOn)
            {
                return ((ICreateStampObject)target).CreatedOn.Ticks.ToString();
            }
            else
            {
                return ((IUpdateStampObject)target).ModifiedOn.Ticks.ToString();
            }
        }

        public void SetValue(object target, object value)
        {
        }
    }

    class StampObjectContractResolver : CamelCasePropertyNamesContractResolver
    {
        public static readonly StampObjectContractResolver Instance = new StampObjectContractResolver();

        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            List<JsonProperty> allProperties = new List<JsonProperty>(base.CreateProperties(type, memberSerialization));
            JsonProperty modifiedOnProp = allProperties.FirstOrDefault(p => p.UnderlyingName.Equals("ModifiedOn", StringComparison.CurrentCultureIgnoreCase) && p.PropertyType == typeof(DateTime));
            JsonProperty createdOnProp = allProperties.FirstOrDefault(p => p.UnderlyingName.Equals("CreatedOn", StringComparison.CurrentCultureIgnoreCase) && p.PropertyType == typeof(DateTime));
            if (modifiedOnProp != null && typeof(IUpdateStampObject).IsAssignableFrom(type))
            {
                modifiedOnProp.ValueProvider = new StampValueProvider(false);
                allProperties.Add(new JsonProperty
                {
                    Order = 0,
                    PropertyType = typeof(string),
                    PropertyName = "_modifiedOn",
                    Readable = true,
                    Writable = false,
                    ValueProvider = new StampDateTimeAsLongValueProvider(false)
                });
            }

            if (createdOnProp != null && typeof(ICreateStampObject).IsAssignableFrom(type))
            {
                createdOnProp.ValueProvider = new StampValueProvider(true);
                allProperties.Add(new JsonProperty
                {
                    Order = 0,
                    PropertyType = typeof(string),
                    PropertyName = "_createdOn",
                    Readable = true,
                    Writable = false,
                    ValueProvider = new StampDateTimeAsLongValueProvider(false)
                });
            }

            return allProperties;
        }
    }

    class UpdateStampObjectConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            bool isIUpdateStampObject = typeof(IUpdateStampObject).IsAssignableFrom(objectType);
            bool isICreateStampObject = typeof(ICreateStampObject).IsAssignableFrom(objectType);
            return isIUpdateStampObject || isICreateStampObject;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);
            var retval = Activator.CreateInstance(objectType);
            serializer.Populate(jsonObject.CreateReader(), retval);
            if (retval is IUpdateStampObject)
            {
                var modifiedOnValue = jsonObject.GetValue("modifiedOn");
                if (modifiedOnValue.Value<object>() != null)
                {
                    var _modifiedOnValue = jsonObject.GetValue("_modifiedOn");
                    ((IUpdateStampObject)retval).ModifiedOn = new DateTime(long.Parse(_modifiedOnValue.Value<string>()));
                }
            }
            if (retval is ICreateStampObject)
            {
                var createdOnValue = jsonObject.GetValue("createdOn");
                if (createdOnValue.Value<object>() != null)
                {
                    var _createdOnValue = jsonObject.GetValue("_createdOn");
                    ((ICreateStampObject)retval).CreatedOn = new DateTime(long.Parse(_createdOnValue.Value<string>()));
                }
            }
            return retval;
        }

        public override bool CanWrite { get { return false; } }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    public interface IUpdateStampObject
    {
        int ModifiedBy { get; set; }
        System.DateTime ModifiedOn { get; set; }
    }

   
    public interface ICreateStampObject
    {
        int CreatedBy { get; set; }
        System.DateTime CreatedOn { get; set; }
    }
}
