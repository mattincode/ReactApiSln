﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Ajax.Utilities;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/employee")]
    public class EmplyeeController : ApiController
    {
        [Route("getAllEmployees")]
        [HttpGet]
        public HttpResponseMessage GetAllEmployees()
        {
            try
            {
                var employees = new List<object>();
                employees.Add(new Employee() { Id = 1, Name = "Nisse Hultman" });
                employees.Add(new Employee() { Id = 2, Name = "Karl Alfred" });
                employees.Add(new Employee() { Id = 3, Name = "Bellman" });
                employees.Add(new Employee() { Id = 4, Name = "Tysken" });
                employees.Add(new Employee() { Id = 5, Name = "Torsken" });
                return Request.CreateResponse(employees);
            }
            catch (Exception e)
            {
                Guid reqId = new Guid();
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, $"{reqId.ToString()}", e);
            }
        }
        
    }

    class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
